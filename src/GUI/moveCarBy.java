package GUI;
import javax.swing.*;
import java.awt.*;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.TimeUnit;

public class moveCarBy extends JPanel implements ActionListener{
    Timer time = new Timer(5,this);
    int x; int xVel ;
    int y; int yVel ;
    int xDest;
    int yDest;
    public moveCarBy(int xArg, int yArg,  int xDestArg,int yDestArg){
        x = xArg;
        y = yArg;
        xVel = 1;
        yVel = 1;
        xDest = xDestArg;
        yDest = yDestArg;

    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.RED);
        g.fillRect(x+30,y+30,60,60);
        g.fillOval(x + 35,y +90,15,15);
        g.fillOval(x + 75,y +90,15,15);

        time.start();
    }
    public void actionPerformed(ActionEvent e) {
        if (x != xDest) {
            x = x + xVel;
            repaint();
        } else {
            if (y != yDest) {
                y = y + yVel;
                repaint();
            }

        }
    }

    public static void main(String[] args) {
        JFrame JFrameObj = new JFrame();
        moveCarBy object = new moveCarBy(50,50, 350,350);
        JFrameObj.add(object);
        JFrameObj.setSize(400,400);
        JFrameObj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JFrameObj.setVisible(true);


    }
}
